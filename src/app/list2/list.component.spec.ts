import { CdkDragDrop, CdkDropList } from '@angular/cdk/drag-drop';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IChuckFact } from '../store/appState.interface';
import { ListComponent } from './list.component';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule],
      declarations: [ListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('isListEmpty', () => {
    it('should return true if facts is undefined', () => {
      expect(component.isListEmpty).toBeTruthy();
    });
    it('should return true if facts is empty array', () => {
      component.facts = [];
      expect(component.isListEmpty).toBeTruthy();
    });
    it('should return false if facts length > 0 ', () => {
      component.facts = [{} as IChuckFact];
      expect(component.isListEmpty).toBeFalse();
    });
  });

  describe('drop()', () => {
    it('should call droped if containers dont match', () => {
      const spy = spyOn(component.dropped, 'emit').and.callFake(() => true);
      const event = {
        container: { id: '1' } as CdkDropList,
        previousContainer: { id: '2' } as CdkDropList,
      } as CdkDragDrop<string>;
      component.drop(event);
      expect(spy).toHaveBeenCalled();
    });

    it('should not call droped if containers dont match', () => {
      const spy = spyOn(component.dropped, 'emit').and.callFake(() => true);
      const container = { id: '1' } as CdkDropList;
      const event = {
        container,
        previousContainer: container,
      } as CdkDragDrop<string>;
      component.drop(event);
      expect(spy).not.toHaveBeenCalled();
    });
  });
});
