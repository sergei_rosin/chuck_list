import { animate, group, style, transition, trigger } from '@angular/animations';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { IChuckFact } from '../store/appState.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('itemAnim', [
      transition(':enter', [
        style({ transform: 'translateY(-20%)' }),
        animate(500),
      ]),
      transition(':leave', [
        group([
          animate(
            '0.5s ease',
            style({
              transform: 'translateY(-20%)',
              height: '0px',
              margin: '0',
              padding: '0',
            })
          ),
          animate('0.5s 0.2s ease', style({ opacity: 0 })),
        ]),
      ]),
    ]),
  ],
})
export class ListComponent {
  @Input() id: string;
  @Input() connectedId: string;
  @Input() facts: IChuckFact[];
  @Input() removable = false;
  @Input() dragable = true;
  @Input() emptyText: string = 'Default text';

  @Output() removed = new EventEmitter();
  @Output() dropped: EventEmitter<number> = new EventEmitter();

  drop(event: CdkDragDrop<string>) {
    if (event.previousContainer !== event.container) {
      this.dropped.emit(event.previousIndex);
    }
  }

  get isListEmpty() {
    return !this.facts || !this.facts.length;
  }
}
