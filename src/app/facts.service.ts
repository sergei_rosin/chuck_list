import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { concatAll, map } from 'rxjs/operators';

import { IChuckFact } from './store/appState.interface';

@Injectable({
  providedIn: 'root',
})
export class FactsService {
  constructor(private http: HttpClient) {}

  makeRequest<T>(url: string): Observable<T> {
    return this.http.get<T>(url, {
      headers: {
        'x-rapidapi-host': 'matchilling-chuck-norris-jokes-v1.p.rapidapi.com',
        'x-rapidapi-key': '53a61af534mshea943f753441ac9p1a9f6ejsnd6468530352c',
        accept: 'application/json',
      },
    });
  }

  getCategories() {
    return this.makeRequest(
      'https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/categories'
    );
  }

  // Could be used to create array of random jokes by category
  getRandomJoke(category, length) {
    return forkJoin(
      [...Array(length).keys()].map(() =>
        this.makeRequest(
          `https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random?category=${category}`
        )
      )
    ).pipe(
      map((v) => [v]),
      concatAll()
    );
  }

  search(category) {
    return this.makeRequest<{ result: IChuckFact[] }>(
      `https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/search?query=${category}`
    ).pipe(map((v) => v.result));
  }
}
