import { NgReduxModule } from '@angular-redux/store';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, throwError } from 'rxjs';

import { AppComponent } from './app.component';
import { FactsService } from './facts.service';
import { ActionType } from './store/actionType.enum';
import { IChuckFact } from './store/appState.interface';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgReduxModule, MatSnackBarModule, BrowserAnimationsModule],
      declarations: [AppComponent],
      providers: [
        {
          provide: FactsService,
          useValue: {
            getCategories: () => of(),
            search: () => of(),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe('ngOnInit()', () => {
    it('should call getCategories and fill category list', fakeAsync(() => {
      const mock = ['test'];
      const spy = spyOn(
        component['factService'],
        'getCategories'
      ).and.returnValue(of(mock));
      component.ngOnInit();
      tick();
      expect(spy).toHaveBeenCalled();
      expect(component.categories).toEqual(mock);
    }));

    it('should call showMessage if request failed', fakeAsync(() => {
      spyOn(component['factService'], 'getCategories').and.returnValue(
        throwError('')
      );
      const spy = spyOn(component, 'showMessage').and.stub();
      component.ngOnInit();
      tick();
      expect(spy).toHaveBeenCalled();
    }));
  });

  it('remove() should call dispatch', fakeAsync(() => {
    const spy = spyOn(component['ngRedux'], 'dispatch').and.stub();
    component.remove('1');
    expect(spy).toHaveBeenCalledWith({
      type: ActionType.REMOVE,
      idToRemove: '1',
    });
  }));

  it('showMessage() should call snackBar.open()', fakeAsync(() => {
    const spy = spyOn(component['snackBar'], 'open').and.stub();
    component.showMessage('kuku');
    expect(spy).toHaveBeenCalledWith('kuku', '', {
      duration: 2000,
    });
  }));

  describe('retrieveFacts()', () => {
    it('should set loading to true', fakeAsync(() => {
      component.retrieveFacts('test');
      expect(component.loading).toBeTrue();
    }));

    it('should set list and set loading to false', fakeAsync(() => {
      const mock = [{ id: 'test' } as IChuckFact];
      const spy = spyOn(component['factService'], 'search').and.returnValue(
        of(mock)
      );
      component.retrieveFacts('test');
      expect(spy).toHaveBeenCalled();
      expect(component.sourceList).toEqual(mock);
      expect(component.loading).toBeFalse();
    }));

    it('should catch error call showMessage() and set loading to false', fakeAsync(() => {
      const spy = spyOn(component['factService'], 'search').and.returnValue(
        throwError('test')
      );
      const spy2 = spyOn(component, 'showMessage').and.stub();
      component.retrieveFacts('test');
      expect(spy).toHaveBeenCalled();
      expect(spy2).toHaveBeenCalled();
      expect(component.loading).toBeFalse();
    }));
  });

  describe('drop()', () => {
    it('should call showMessage if list already includes new item', () => {
      spyOn(component['ngRedux'], 'getState').and.returnValue({
        customList: [{ id: 'test' } as IChuckFact],
      });
      const spy = spyOn(component, 'showMessage').and.stub();
      component.sourceList = [{ id: 'test' } as IChuckFact];
      component.drop(0);
      expect(spy).toHaveBeenCalled();
    });

    it('should call dispatch if list already doesnt have this item', () => {
      spyOn(component['ngRedux'], 'getState').and.returnValue({
        customList: [{ id: 'test' } as IChuckFact],
      });
      const spy = spyOn(component['ngRedux'], 'dispatch').and.stub();
      component.sourceList = [{ id: 'test2' } as IChuckFact];
      component.drop(0);
      expect(spy).toHaveBeenCalledWith({
        type: ActionType.ADD,
        newItem: { id: 'test2' },
      });
    });
  });
});
