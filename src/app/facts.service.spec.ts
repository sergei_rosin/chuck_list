import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { FactsService } from './facts.service';

describe('FactsService', () => {
  let service: FactsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: {
            get: () => true,
          },
        },
      ],
    });
    service = TestBed.inject(FactsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('makeRequest() should call http with headers ', () => {
    const spy = spyOn(service['http'], 'get').and.returnValue(of());
    service.makeRequest('test');
    expect(spy).toHaveBeenCalledWith('test', {
      headers: {
        'x-rapidapi-host': 'matchilling-chuck-norris-jokes-v1.p.rapidapi.com',
        'x-rapidapi-key': '53a61af534mshea943f753441ac9p1a9f6ejsnd6468530352c',
        accept: 'application/json',
      },
    });
  });

  it('search() should call makeRequest() ', (done) => {
    const spy = spyOn(service, 'makeRequest').and.returnValue(
      of({ id: 'test' })
    );
    service.search('test').subscribe(() => {
      expect(spy).toHaveBeenCalled();
      done();
    });
  });

  it('getCategories() should call makeRequest() ', (done) => {
    const spy = spyOn(service, 'makeRequest').and.returnValue(of(['test']));
    service.getCategories().subscribe(() => {
      expect(spy).toHaveBeenCalled();
      done();
    });
  });

  it('getRandomJoke() should call makeRequest() ', (done) => {
    const spy = spyOn(service, 'makeRequest').and.returnValue(
      of({ result: [{ id: 'test' }] })
    );
    service.getRandomJoke('test', 1).subscribe(() => {
      expect(spy).toHaveBeenCalled();
      done();
    });
  });
});
