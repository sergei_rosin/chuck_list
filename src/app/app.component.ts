import { NgRedux, select } from '@angular-redux/store';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

import { FactsService } from './facts.service';
import { ActionType } from './store/actionType.enum';
import { IAppState, IChuckFact } from './store/appState.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @select() customList$: Observable<IChuckFact[]>;
  sourceList: IChuckFact[];
  categories: string[];
  loading = false;

  constructor(
    private ngRedux: NgRedux<IAppState>,
    private factService: FactsService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.factService.getCategories().subscribe(
      (res: []) => {
        this.categories = res;
      },
      () => {
        this.showMessage('Error occured, please refresh the page');
      }
    );
  }

  retrieveFacts(category: string) {
    this.loading = true;
    this.factService.search(category).subscribe(
      (facts) => {
        this.sourceList = facts;
        this.loading = false;
      },
      () => {
        this.showMessage('Error occured, please try again');
        this.loading = false;
      }
    );
  }

  drop(indexToTransfer: number) {
    const newItem = this.sourceList[indexToTransfer];
    const filtered = this.ngRedux
      .getState()
      .customList.filter((i) => i.id === newItem.id);
    if (filtered.length) {
      this.showMessage('You have already added this fact!');
      return;
    }
    this.ngRedux.dispatch({
      type: ActionType.ADD,
      newItem,
    });
    this.showMessage('Fact was added to your list!');
  }

  showMessage(msg: string) {
    this.snackBar.open(msg, '', {
      duration: 2000,
    });
  }

  remove(idToRemove: string) {
    this.ngRedux.dispatch({ type: ActionType.REMOVE, idToRemove });
  }
}
