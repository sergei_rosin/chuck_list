export interface IChuckFact {
  categories?: string[];
  icon_url: string;
  id: string;
  url: string;
  value: string;
}

export interface IAppState {
  customList: IChuckFact[];
}
