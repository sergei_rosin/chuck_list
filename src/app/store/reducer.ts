import { Action } from 'redux';
import { IAppState, IChuckFact } from './appState.interface';
import { ActionType } from './actionType.enum';

type PayloadAction = Action<ActionType> & {
  newItem?: IChuckFact;
  idToRemove?: string;
};

const reducer = (state: IAppState, action: PayloadAction) => {
  let { customList } = state;
  switch (action.type) {
    case ActionType.ADD:
      customList.push(action.newItem);
      break;
    case ActionType.REMOVE:
      customList = customList.filter((v) => v.id !== action.idToRemove);
      break;
  }
  customList = customList.sort((a, b) => (a.value > b.value ? 1 : -1));
  localStorage.setItem('chuck-list', JSON.stringify(customList));
  return {
    customList,
  };
};

export { reducer };
