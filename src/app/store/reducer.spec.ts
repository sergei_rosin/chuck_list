import { ActionType } from './actionType.enum';
import { IChuckFact } from './appState.interface';
import { reducer } from './reducer';

describe('reducer()', () => {
  it('should add item to list', () => {
    const store = {
      customList: [{ id: 'test' } as IChuckFact],
    };
    const action = {
      type: ActionType.ADD,
      newItem: { id: 'test2' } as IChuckFact,
    };
    expect(reducer(store, action).customList.length).toEqual(2);
    expect(reducer(store, action).customList).toContain(action.newItem);
  });

  it('should remove item from list', () => {
    const store = {
      customList: [{ id: 'test' } as IChuckFact],
    };
    const action = {
      type: ActionType.REMOVE,
      idToRemove: 'test',
    };
    expect(reducer(store, action).customList.length).toEqual(0);
  });

  it('should sort list', () => {
    let store = {
      customList: [{ value: 'b' } as IChuckFact],
    };
    const action = {
      type: ActionType.ADD,
      newItem: { value: 'a' } as IChuckFact,
    };
    const action2 = {
      type: ActionType.ADD,
      newItem: { value: 'c' } as IChuckFact,
    };
    store = reducer(store, action);
    expect(reducer(store, action2).customList).toEqual([
      { value: 'a' } as IChuckFact,
      { value: 'b' } as IChuckFact,
      { value: 'c' } as IChuckFact,
    ]);
  });
});
