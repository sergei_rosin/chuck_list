# ChuckList

Disclaimer: I didnt have a lot of experience with Redux, since we didnt use it in our projects. So probably implementation doesnt follow the best practices.

Potential improvements:

1. Adding pagination to sourcelist
2. Adding interceptor to http requests to catch errors and show error notification (can also make a retry button to make re-request)
3. 


## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
